#!/bin/bash

echo "$bamboo_bitbucket_user"

UPSTREAM_REPO="https://github.com/romed/swaro_testcase_upstream.git"
OUR_REPO="https://$bamboo_bitbucket_user:$bamboo_bitbucket_password@bitbucket.org/romed/swaro_test_ours.git"

UPSTREAM_LATEST_TAG_HASH=$(git -c 'versionsort.suffix=-' ls-remote --tags --sort='v:refname' $UPSTREAM_REPO | tail -1 | cut -f 1)

OURS_LATEST_TAG_HASH=$(git -c 'versionsort.suffix=-' ls-remote --tags --sort='v:refname' $OUR_REPO | tail -1 | cut -f 1)

echo "$UPSTREAM_LATEST_TAG_HASH"
echo "$OURS_LATEST_TAG_HASH"

if [ "$UPSTREAM_LATEST_TAG_HASH" != "$OURS_LATEST_TAG_HASH" ]
then
	echo "Need to rebase!"
	WD=$(mktemp -d --tmpdir=/tmp auto_rebase_wd.XXXX)
	cd $WD
	pwd
	git clone $OUR_REPO
	cd $(echo $OUR_REPO | cut -d '/' -f 2 | cut -d '.' -f 1)
	git remote add upstream $UPSTREAM_REPO
	git fetch upstream
#	git checkout origin/main
	git merge upstream/main
	git push -f origin main --tags
	git checkout our
	git rebase origin/main 
	git push -f origin our --tags
else
	echo "Newest tag/release has the same hash in upstream and our repo. No need to rebase."
fi
